import requests
import json
import sys
import os
from time import sleep
from flask import Flask
from flask import request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

login_url = os.environ.get('AUTH_URL')
tangogql_url =  os.environ.get('TANGOGQL_URL')
webjive_username = os.environ.get('WEBJIVE_USER')
webjive_password = os.environ.get('WEBJIVE_PASS')

def login():
    jsonLogin={"username":webjive_username,"password":webjive_password}
    r = requests.post(url=login_url, json=jsonLogin)
    print(str(r))
    return r.cookies.get_dict()['webjive_jwt']

@app.route('/', methods=['GET'])
def base():
    return "Hello from tango-gql proxy!"

@app.route('/login_webjive', methods=['POST'])
def login_webjive():
    jsonLogin={"username":webjive_username,"password":webjive_password}
    r = requests.post(url=login_url, json=jsonLogin)
    response = app.response_class(
        response="webjive login result",
        status=r.status_code
    )
    response.set_cookie('webjive_jwt', r.cookies.get_dict()['webjive_jwt'])
    return response

@app.route('/mutation', methods=['POST'])
def mutation():
    app.logger.info(request.values)
    app.logger.info(request.data)
    jsonInput = json.loads(request.data)
    app.logger.info(json.dumps(jsonInput))
    app.logger.info(jsonInput['mutation'])
    cookies = {'webjive_jwt': login()}
    r = requests.post(url=tangogql_url, json=json.loads(jsonInput['mutation']), cookies=cookies)
    print(str(r))
    app.logger.info("response from tangogql=" + r.text)
    response = app.response_class(
        response=json.dumps(json.loads(r.text)),
        status=200,
        mimetype='application/json'
    )
    return response
